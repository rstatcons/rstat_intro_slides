library(DiagrammeR)
numbofexp <- 3
numboflibs <- 5
numbofdevs <- 7

expnodes <- paste0("exp", seq_len(numbofexp))
explabels <- paste0("Member", seq_len(numbofexp))
libnodes <- paste0("lib",  seq_len(numboflibs))
liblabels <- paste0("Package",  seq_len(numboflibs))
devnodes <- paste0("dev", seq_len(numbofdevs))
devlabels <- paste0("Developer", seq_len(numbofdevs))

nodes <-
  create_node_df(n = sum(2, numbofexp, numboflibs, numbofdevs),
              node = c("base", "board", expnodes, libnodes, devnodes),
               label = c("R core", "R core board", explabels, liblabels, devlabels),
               type = "lower",
               # style = "filled",
               color = "black",
               shape = c("circle", "rectangle", rep("triangle", numbofexp),
                         rep("circle", numboflibs), rep("triangle", numbofdevs)) #,
               #                y = c(200, 100, rep(20, numbofexp), rep(300, numboflibs), rep(400, numbofdevs)),
               #                x = 400
  )


node2id <- function(node, nodes_df = nodes) {
  if(length(node) > 1L) 
    return(vapply(node, node2id, integer(1L), nodes_df))
  
  nodes_df$id[nodes_df$node == node]
}

devslibsedges <- list(from = sample(devnodes, numbofdevs * 2, replace = T),
                      to   = sample(libnodes, numbofdevs * 2, replace = T))
edges <-
  create_edge_df(from = node2id(c("board", expnodes, libnodes, devslibsedges[['from']])),
               to = node2id(c("base", rep("board", numbofexp), 
                              rep("base", numboflibs), devslibsedges[['to']])))

graph <- create_graph(nodes_df = nodes,
                      edges_df = edges)
                      # graph_attrs = c("layout = neato",
                      #                 "overlap = false"),
                      # node_attrs = c("fontsize = 7",
                      #                "width = .8",
                      #                "height = .8",
                      #                "fixedsize = true"),
                      # edge_attrs = c("arrowsize = 0.5"))

cat(render_graph(graph, output = "SVG"), file = "figs/r_scheme.svg")
